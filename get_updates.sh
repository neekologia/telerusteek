#!/bin/bash
set -e

curl -sS "https://api.telegram.org/bot$TOKEN/setWebhook" >/dev/null

offset=0
while true; do
	updates=$(curl -sS "https://api.telegram.org/bot$TOKEN/getUpdates" --data "offset=$offset")
	newoffset=$(jq '.result[-1].update_id'<<<"$updates")

	# zio pera
	jq -cj '.result[] | (., "\u0000")' <<<"$updates" | \
		while IFS= read -r -d '' update; do
			printf "%s\n" "$update"
			printf "%s\n" "$update" >&2
		done

	if [ "$newoffset" != "null" ]; then 
		offset=$(("$newoffset" + 1))
	fi

	sleep 1
done


