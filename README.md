# telerusteek

telerusteek is a general purpose telegram bot, example with current code: [telerusteek](https://t.me/neekrustbot)

## Requirements

- [Rust](https://www.rust-lang.org/tools/install)

## Install

```bash
cargo build --release
```

## Usage

create a webhook.yaml as:
```yaml
# address and port to bind
host:
port:
```
and an api.yaml as:
```yaml
# telegram bot token
token:
# telegram bot server ip and port
host:
port:
```

to start the bot simply run:
```bash
cargo run --release
```

## License

[GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html)
