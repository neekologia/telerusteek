use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};

#[derive(Debug, Deserialize)]
pub struct Api {
	pub token: String,
	pub host: String,
	pub port: String,
	pub tls: bool
}

#[derive(Deserialize, Debug)]
pub struct Input {
	pub update_id: u64,
	pub message: Option<Message>,
}
#[derive(Deserialize, Serialize, Debug)]
pub struct Message {
	pub message_id: u64,
	pub from: Option<User>,
	pub sender_chat: Option<Chat>,
	pub date: u64,
	pub chat: Chat,
	pub forward_from: Option<User>,
	pub forward_from_chat: Option<Map<String, Value>>,
	pub forward_from_message_id: Option<u64>,
	pub forward_signature: Option<String>,
	pub forward_sender_name: Option<String>,
	pub forward_date: Option<u64>,
	pub reply_to_message: Option<ReplyToMessage>,
	pub via_bot: Option<User>,
	pub edit_date: Option<u64>,
	pub media_group_id: Option<String>,
	pub author_signature: Option<String>,
	pub text: Option<String>,
	pub entities: Option<Vec<Map<String, Value>>>,
	pub animation: Option<Animation>,
	pub audio: Option<Audio>,
	pub document: Option<Map<String, Value>>,
	pub photo: Option<Vec<PhotoSize>>,
	pub sticker: Option<Sticker>,
	pub video: Option<Video>,
	pub video_note: Option<Map<String, Value>>,
	pub voice: Option<Map<String, Value>>,
	pub caption: Option<String>,
	pub caption_entities: Option<Vec<Map<String, Value>>>,
	pub contact: Option<Map<String, Value>>,
	pub dice: Option<Map<String, Value>>,
	pub game: Option<Map<String, Value>>,
	pub poll: Option<Map<String, Value>>,
	pub venue: Option<Map<String, Value>>,
	pub location: Option<Map<String, Value>>,
	pub new_chat_members: Option<Vec<Map<String, Value>>>,
	pub left_chat_member: Option<Map<String, Value>>,
	pub new_chat_title: Option<String>,
	pub new_chat_photo: Option<Vec<Map<String, Value>>>,
	pub delete_chat_photo: Option<bool>,
	pub group_chat_created: Option<bool>,
	pub supergroup_chat_created: Option<bool>,
	pub channel_chat_created: Option<bool>,
	pub message_auto_delete_timer_changed: Option<Map<String, Value>>,
	pub migrate_to_chat_id: Option<u64>,
	pub migrate_from_chat_id: Option<u64>,
	pub pinned_message: Option<Map<String, Value>>,
	pub invoice: Option<Map<String, Value>>,
	pub successful_payment: Option<Map<String, Value>>,
	pub connected_website: Option<String>,
	pub passport_data: Option<Map<String, Value>>,
	pub proximity_alert_triggered: Option<Map<String, Value>>,
	pub voice_chat_scheduled: Option<Map<String, Value>>,
	pub voice_chat_started: Option<Map<String, Value>>,
	pub voice_chat_ended: Option<Map<String, Value>>,
	pub voice_chat_participants_invited: Option<Map<String, Value>>,
	pub reply_markup: Option<Map<String, Value>>,
}
#[derive(Deserialize, Serialize, Debug)]
pub struct ReplyToMessage {
	pub message_id: u64,
	pub from: Option<User>,
	pub sender_chat: Option<Chat>,
	pub date: u64,
	pub chat: Chat,
	pub forward_from: Option<User>,
	pub forward_from_chat: Option<Map<String, Value>>,
	pub forward_from_message_id: Option<u64>,
	pub forward_signature: Option<String>,
	pub forward_sender_name: Option<String>,
	pub forward_date: Option<u64>,
	pub via_bot: Option<User>,
	pub edit_date: Option<u64>,
	pub media_group_id: Option<String>,
	pub author_signature: Option<String>,
	pub text: Option<String>,
	pub entities: Option<Vec<Map<String, Value>>>,
	pub animation: Option<Animation>,
	pub audio: Option<Audio>,
	pub document: Option<Map<String, Value>>,
	pub photo: Option<Vec<PhotoSize>>,
	pub sticker: Option<Sticker>,
	pub video: Option<Video>,
	pub video_note: Option<Map<String, Value>>,
	pub voice: Option<Map<String, Value>>,
	pub caption: Option<String>,
	pub caption_entities: Option<Vec<Map<String, Value>>>,
	pub contact: Option<Map<String, Value>>,
	pub dice: Option<Map<String, Value>>,
	pub game: Option<Map<String, Value>>,
	pub poll: Option<Map<String, Value>>,
	pub venue: Option<Map<String, Value>>,
	pub location: Option<Map<String, Value>>,
	pub new_chat_members: Option<Vec<Map<String, Value>>>,
	pub left_chat_member: Option<Map<String, Value>>,
	pub new_chat_title: Option<String>,
	pub new_chat_photo: Option<Vec<Map<String, Value>>>,
	pub delete_chat_photo: Option<bool>,
	pub group_chat_created: Option<bool>,
	pub supergroup_chat_created: Option<bool>,
	pub channel_chat_created: Option<bool>,
	pub message_auto_delete_timer_changed: Option<Map<String, Value>>,
	pub migrate_to_chat_id: Option<u64>,
	pub migrate_from_chat_id: Option<u64>,
	pub pinned_message: Option<Map<String, Value>>,
	pub invoice: Option<Map<String, Value>>,
	pub successful_payment: Option<Map<String, Value>>,
	pub connected_website: Option<String>,
	pub passport_data: Option<Map<String, Value>>,
	pub proximity_alert_triggered: Option<Map<String, Value>>,
	pub voice_chat_scheduled: Option<Map<String, Value>>,
	pub voice_chat_started: Option<Map<String, Value>>,
	pub voice_chat_ended: Option<Map<String, Value>>,
	pub voice_chat_participants_invited: Option<Map<String, Value>>,
	pub reply_markup: Option<Map<String, Value>>,
}
#[derive(Deserialize, Serialize, Debug)]
pub struct User {
	pub id: i64,
	pub is_bot: bool,
	pub first_name: String,
	pub last_name: Option<String>,
	pub username: Option<String>,
	pub language_code: Option<String>,
	pub can_join_groups: Option<bool>,
	pub can_read_all_group_messages: Option<bool>,
	pub supports_inline_queries: Option<bool>,
}
#[derive(Deserialize, Serialize, Debug)]
pub struct Chat {
	pub id: i64,
	pub r#type: String,
}
#[derive(Deserialize, Serialize, Debug)]
pub struct PhotoSize {
	pub file_id: String,
	pub file_unique_id: String,
	pub width: u64,
	pub height: u64,
	pub file_size: u64,
}
#[derive(Deserialize, Serialize, Debug)]
pub struct Sticker {
	pub file_id: String,
	pub file_unique_id: String,
	pub width: u64,
	pub height: u64,
	pub is_animated: bool,
	pub is_video: bool,
	pub r#type: Option<String>,
	pub thumbnail: Option<Map<String, Value>>,
	pub emoji: Option<String>,
	pub set_name: Option<String>,
	pub premium_animation: Option<Map<String, Value>>,
	pub mask_position: Option<Map<String, Value>>,
	pub custom_emoji_id: Option<String>,
	pub needs_repainting: Option<bool>,
	pub file_size: Option<u64>,
}
#[derive(Deserialize, Serialize, Debug)]
pub struct Audio {
	pub file_id: String,
	pub file_unique_id: String,
	pub duration: u64,
	pub performer: Option<String>,
	pub title: Option<String>,
	pub file_name: Option<String>,
	pub mime_type: Option<String>,
	pub file_size: Option<u64>,
	pub thumbnail: Option<PhotoSize>,
}
#[derive(Deserialize, Serialize, Debug)]
pub struct Video {
	pub file_id: String,
	pub file_unique_id: String,
	pub width: u64,
	pub height: u64,
	pub duration: u64,
	pub thumbnail: Option<PhotoSize>,
	pub file_name: Option<String>,
	pub mime_type: Option<String>,
	pub file_size: Option<u64>,
}
#[derive(Deserialize, Serialize, Debug)]
pub struct Animation {
	pub file_id: String,
	pub file_unique_id: String,
	pub width: u64,
	pub height: u64,
	pub duration: u64,
	pub thumbnail: Option<PhotoSize>,
	pub file_name: Option<String>,
	pub mime_type: Option<String>,
	pub file_size: Option<u64>,
}

#[derive(Deserialize, Serialize, Debug, Default)]
pub struct ChatPermissions {
	pub can_send_messages: Option<bool>,
	pub can_send_media_messages: Option<bool>,
	pub can_send_polls: Option<bool>,
	pub can_send_other_messages: Option<bool>,
	pub can_add_web_page_previews: Option<bool>,
	pub can_change_info: Option<bool>,
	pub can_invite_users: Option<bool>,
	pub can_pin_messages: Option<bool>,
}

#[derive(Deserialize, Debug)]
pub struct Response {
	pub result: Value,
	pub ok: bool,
	pub description: Option<String>,
	pub parameters: Option<ResponseParameters>,
}
#[derive(Deserialize, Debug)]
pub struct ResponseParameters {
	pub migrate_to_chat_id: Option<u64>,
	pub retry_after: Option<u64>,
}

#[derive(Deserialize, Debug)]
pub struct ChatInviteLink {
	pub invite_link: String,
	pub creator: User,
	pub creates_join_request: bool,
	pub is_primary: bool,
	pub is_revoked: bool,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub name: Option<String>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub expire_date: Option<u64>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub member_limit: Option<u64>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub pending_join_request_count: Option<u64>,
}
#[derive(Deserialize, Debug)]
pub struct ChatMember {
	pub status: String,
	pub user: User,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub is_anonymous: bool,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub custom_title: Option<String>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_be_edited: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_manage_chat: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_delete_messages: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_manage_video_chats: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_restrict_members: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_promote_members: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_change_info: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_invite_users: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_post_messages: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_edit_messages: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_pin_messages: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_manage_topics: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub is_member: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_send_messages: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_send_audios: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_send_documents: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_send_photos: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_send_videos: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_send_video_notes: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_send_polls: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_send_other_messages: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub can_add_web_page_previews: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub until_date: Option<u64>,
}
#[derive(Deserialize, Debug)]
pub struct File {
	pub file_id: String,
	pub file_unique_id: String,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub file_size: Option<u64>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub file_path: Option<String>,
}
