use crate::commands::types::BotCmd;
use crate::utils::utils;
use crate::*;
use serde_json::from_str;

fn parse_command(message: &Message) -> bool {
	let Some(ref text) = message.text else {
		return false;
	};

	if text.starts_with([';', '/']) {
		let Some(cmd) = BotCmd::get_cmd(message) else {
			return false;
		};
		match_command(message, cmd);
		return true;
	} else {
		match_text(message, text);
		return false;
	}
}

fn parse_message(message: &Message) {
	if !parse_command(message) {
		match_chat_id(message);
		match_bot_chat_id(message);
	}
}

fn update_db(message: &Message) {
	match &message.from {
		Some(user) => {
			let chat_path = format!("db/{}/", message.chat.id);
			if !std::path::Path::new(&chat_path).exists() {
				std::fs::create_dir_all(&chat_path).unwrap();
			}
			let Ok(file) = utils::w_create_truncate(&format!("{}{}", chat_path, user.id)) else {
				return;
			};
			serde_json::to_writer(file, &user).unwrap();
		}
		None => (),
	}
}

pub fn get_message(data: &str) {
	let input: Input = from_str(data).unwrap();
	if let Some(message) = input.message {
		update_db(&message);
		parse_message(&message);
	}
}
