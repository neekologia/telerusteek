use std::error::Error;

use crate::requests::post_with_wait;
use crate::types::{ChatMember, ChatPermissions, File, Message, Response, User};
use serde::Serialize;

#[derive(Serialize, Debug, Default)]
pub struct GetMe {}

#[derive(Serialize, Debug, Default)]
pub struct SendMessage {
	pub chat_id: i64,
	pub text: String,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub reply_to_message_id: Option<u64>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub parse_mode: Option<String>,
}

#[derive(Serialize, Debug, Default)]
pub struct SendPhoto {
	pub chat_id: i64,
	pub photo: String,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub reply_to_message_id: Option<u64>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub caption: Option<String>,
}

#[derive(Serialize, Debug, Default)]
pub struct SendSticker {
	pub chat_id: i64,
	pub sticker: String,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub reply_to_message_id: Option<u64>,
}

#[derive(Serialize, Debug, Default)]
pub struct SendVideo {
	pub chat_id: i64,
	pub video: String,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub reply_to_message_id: Option<u64>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub caption: Option<String>,
}

#[derive(Serialize, Debug, Default)]
pub struct SendAudio {
	pub chat_id: i64,
	pub audio: String,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub reply_to_message_id: Option<u64>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub caption: Option<String>,
}

#[derive(Serialize, Debug, Default)]
pub struct SendAnimation {
	pub chat_id: i64,
	pub animation: String,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub reply_to_message_id: Option<u64>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub caption: Option<String>,
}

#[derive(Serialize, Debug, Default)]
pub struct SendDocument {
	pub chat_id: i64,
	pub document: String,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub thumb: Option<String>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub caption: Option<String>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub parse_mode: Option<String>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub reply_to_message_id: Option<u64>,
}

#[derive(Serialize, Debug, Default)]
pub struct BanChatMember {
	pub chat_id: i64,
	pub user_id: i64,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub until_date: Option<u64>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub revoke_messages: Option<bool>,
}

#[derive(Serialize, Debug, Default)]
pub struct RestrictChatMember {
	pub chat_id: i64,
	pub user_id: i64,
	pub permissions: ChatPermissions,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub until_date: Option<u64>,
}

#[derive(Serialize, Debug, Default)]
pub struct GetChatMember {
	pub chat_id: i64,
	pub user_id: i64,
}

#[derive(Serialize, Debug, Default)]
pub struct GetFile {
	pub file_id: String,
}

impl GetMe {
	pub fn request() -> User {
		let method = "/getMe";
		let object = serde_json::to_string(&GetMe {}).unwrap();
		let file = None;

		let response: User =
			serde_json::from_value(post_with_wait(method, object, file).unwrap().result).unwrap();
		response
	}
}

impl SendMessage {
	pub fn reply(message: &Message) -> Self {
		Self {
			chat_id: message.chat.id,
			reply_to_message_id: Some(message.message_id),
			..Default::default()
		}
	}

	pub fn request(&self) -> Result<Response, Box<dyn Error>> {
		let method = "/sendMessage";
		let object = serde_json::to_string(self).unwrap();
		let file = None;

		return post_with_wait(method, object, file);
	}
}

impl SendPhoto {
	pub fn reply(message: &Message) -> Self {
		Self {
			chat_id: message.chat.id,
			reply_to_message_id: Some(message.message_id),
			..Default::default()
		}
	}

	pub fn request(&self) -> Result<Response, Box<dyn Error>> {
		let method = "/sendPhoto";
		let object = serde_json::to_string(self).unwrap();
		let file = if std::path::Path::new(&self.photo).exists() {
			Some(self.photo.clone())
		} else {
			None
		};

		return post_with_wait(method, object, file);
	}
}

impl SendSticker {
	pub fn reply(message: &Message) -> Self {
		Self {
			chat_id: message.chat.id,
			reply_to_message_id: Some(message.message_id),
			..Default::default()
		}
	}

	pub fn request(&self) -> Result<Response, Box<dyn Error>> {
		let method = "/sendSticker";
		let object = serde_json::to_string(self).unwrap();
		let file = if std::path::Path::new(&self.sticker).exists() {
			Some(self.sticker.clone())
		} else {
			None
		};

		return post_with_wait(method, object, file);
	}
}

impl SendVideo {
	pub fn reply(message: &Message) -> Self {
		Self {
			chat_id: message.chat.id,
			reply_to_message_id: Some(message.message_id),
			..Default::default()
		}
	}

	pub fn request(&self) -> Result<Response, Box<dyn Error>> {
		let method = "/sendVideo";
		let object = serde_json::to_string(self).unwrap();
		let file = if std::path::Path::new(&self.video).exists() {
			Some(self.video.clone())
		} else {
			None
		};

		return post_with_wait(method, object, file);
	}
}

impl SendAudio {
	pub fn reply(message: &Message) -> Self {
		Self {
			chat_id: message.chat.id,
			reply_to_message_id: Some(message.message_id),
			..Default::default()
		}
	}

	pub fn request(&self) -> Result<Response, Box<dyn Error>> {
		let method = "/sendAudio";
		let object = serde_json::to_string(self).unwrap();
		let file = if std::path::Path::new(&self.audio).exists() {
			Some(self.audio.clone())
		} else {
			None
		};

		return post_with_wait(method, object, file);
	}
}

impl SendAnimation {
	pub fn reply(message: &Message) -> Self {
		Self {
			chat_id: message.chat.id,
			reply_to_message_id: Some(message.message_id),
			..Default::default()
		}
	}

	pub fn request(&self) -> Result<Response, Box<dyn Error>> {
		let method = "/sendAnimation";
		let object = serde_json::to_string(self).unwrap();
		let file = if std::path::Path::new(&self.animation).exists() {
			Some(self.animation.clone())
		} else {
			None
		};

		return post_with_wait(method, object, file);
	}
}

impl SendDocument {
	pub fn reply(message: &Message) -> Self {
		Self {
			chat_id: message.chat.id,
			reply_to_message_id: Some(message.message_id),
			..Default::default()
		}
	}

	pub fn request(&self) -> Result<Response, Box<dyn Error>> {
		let method = "/sendDocument";
		let object = serde_json::to_string(self).unwrap();
		let file = if std::path::Path::new(&self.document).exists() {
			Some(self.document.clone())
		} else {
			None
		};

		return post_with_wait(method, object, file);
	}
}

impl BanChatMember {
	pub fn request(&self) -> Result<Response, Box<dyn Error>> {
		let method = "/banChatMember";
		let object = serde_json::to_string(self).unwrap();
		let file = None;
		return post_with_wait(method, object, file);
	}
}

impl RestrictChatMember {
	pub fn request(&self) -> Result<Response, Box<dyn Error>> {
		let method = "/restrictChatMember";
		let object = serde_json::to_string(self).unwrap();
		let file = None;

		return post_with_wait(method, object, file);
	}
}

impl GetChatMember {
	pub fn request(&self) -> Option<ChatMember> {
		let method = "/getChatMember";
		let object = serde_json::to_string(self).unwrap();
		let file = None;

		return serde_json::from_value(post_with_wait(method, object, file).unwrap().result)
			.unwrap();
	}
}

impl GetFile {
	pub fn request(&self) -> File {
		let method = "/getFile";
		let object = serde_json::to_string(self).unwrap();
		let file = None;

		return serde_json::from_value(post_with_wait(method, object, file).unwrap().result)
			.unwrap();
	}
}
