use crate::commands::types::ChatArg;
use crate::methods::*;
use crate::types::*;
use crate::utils::lstatic::BOT_CHATS_DIR;
use crate::utils::utils;
use std::error::Error;
use std::fs;
use std::io::Seek;
use std::io::Write;
use std::path::Path;
use std::time::{SystemTime, UNIX_EPOCH};

use super::types::BotCmd;
use super::types::ConvertArg;
use super::types::DataArg;

#[derive(Debug)]
struct BotChat {
	host: i64,
	users: Vec<i64>,
}

fn chars_bytes_usize_sum(vec_str: Vec<&str>) -> Vec<usize> {
	let mut list_n: Vec<usize> = vec![0, 0];
	let mut n: usize = 0;
	for x in 0..vec_str.len() {
		for y in vec_str[x].as_bytes().iter() {
			n += *y as usize;
		}
		list_n[x] = n;
		n = 0;
	}
	list_n
}

fn sign_split_vec_str<'a>(
	reply_text: &'a str,
	signs: Vec<&'a str>,
) -> Option<(Vec<&'a str>, &'a str)> {
	let mut vec_str: Option<(Vec<&str>, &str)> = None;
	for sign in signs.iter() {
		if reply_text.contains(sign) {
			match reply_text.split_once(sign) {
				Some(split) => {
					vec_str = Some((vec![split.0, split.1], *sign));
					break;
				}
				None => (),
			}
		}
	}
	vec_str
}

fn zip_file<W: Write + Seek>(zip: &mut zip::ZipWriter<W>, file: &str) {
	let options =
		zip::write::FileOptions::default().compression_method(zip::CompressionMethod::Stored);
	zip.start_file(file, options).unwrap();
	let read = std::fs::read(file).unwrap();
	let buf = &read[..];
	zip.write_all(buf).unwrap();
}

fn cmd(command: &str, args: Vec<&str>, stdin_str: Option<String>) -> String {
	use std::process::{Command, Stdio};
	match stdin_str {
		Some(stdin_str) => {
			let mut cmd = Command::new(command)
				.args(args)
				.stdin(Stdio::piped())
				.stdout(Stdio::piped())
				.stderr(Stdio::piped())
				.spawn()
				.unwrap();
			let mut stdin = cmd.stdin.take().unwrap();
			std::thread::spawn(move || {
				stdin.write_all(stdin_str.as_bytes()).ok();
			});
			let output = cmd.wait_with_output().unwrap();
			format!(
				"{}{}",
				String::from_utf8_lossy(&output.stdout),
				String::from_utf8_lossy(&output.stderr)
			)
		}
		None => {
			let cmd = Command::new(command)
				.args(args)
				.stdout(Stdio::piped())
				.stderr(Stdio::piped())
				.spawn()
				.unwrap();
			let output = cmd.wait_with_output().unwrap();
			format!(
				"{}{}",
				String::from_utf8_lossy(&output.stdout),
				String::from_utf8_lossy(&output.stderr)
			)
		}
	}
}

fn get_audio_codec(filename: &str) -> String {
	let mut args: Vec<&str> = "-v error -select_streams a:0 -show_entries stream=codec_name -of default=noprint_wrappers=1:nokey=1".split_whitespace().collect();
	args.insert(args.len(), filename);
	return cmd("ffprobe", args, None);
}

struct MediaConv {
	file_name: String,
	dir: String,
}
fn get_media_conv(reply: &ReplyToMessage, convert_to: &ConvertArg) -> Option<MediaConv> {
	let reply_media_id = {
		if let Some(audio) = reply.audio.as_ref() {
			&audio.file_id
		} else if let Some(photos) = reply.photo.as_ref() {
			&photos.get(photos.len()-1).unwrap().file_id
		} else if let Some(sticker) = reply.sticker.as_ref() {
			&sticker.file_id
		} else if let Some(video) = reply.video.as_ref() {
			&video.file_id
		} else {
			return None;
		}
	};
	let Some(mut file_path) = GetFile{file_id: reply_media_id.to_string()}.request().file_path else {
		return None;
	};
	let Some(file_ext) = file_path.split('.').last() else {
		return None;
	};
	match file_ext {
		"mp4" | "mov" | "gif" | "mkv" => {
			match convert_to {
				ConvertArg::Photo | ConvertArg::Sticker => {
					file_path = format!("{file_path}[0]");
				}
				_ => (),
			}
		}
		_ => (),
	}
	let tmpdir = format!("/tmp/{}{}", reply.chat.id, reply.message_id);
	fs::create_dir_all(tmpdir.clone()).unwrap();
	let mut tmpfile_name = format!("{tmpdir}/convert");
	match convert_to {
		ConvertArg::Audio => {
			tmpfile_name = format!("{tmpfile_name}.mp3");
		}
		ConvertArg::Video | ConvertArg::Animation => {
			tmpfile_name = format!("{tmpfile_name}.mp4");
		}
		ConvertArg::Photo => {
			tmpfile_name = format!("{tmpfile_name}.jpg");
		}
		ConvertArg::Sticker => {
			tmpfile_name = format!("{tmpfile_name}.webp");
		}
	}
	let output = match convert_to {
		ConvertArg::Animation => {
			cmd("ffmpeg", vec!["-v",
				"error", "-y",
				"-i", &file_path.strip_suffix("[0]").unwrap(),
				"-an", "-vcodec", "copy", tmpfile_name.as_str()], None)
		}
		ConvertArg::Audio => {
			let audio_codec = get_audio_codec(&file_path);
			let acodec_parameter = if audio_codec.as_str() == "mp3" {
				"copy"
			} else {
				"libmp3lame"
			};
			cmd("ffmpeg", vec!["-v",
				"error", "-y",
				"-i", &file_path,
				"-acodec", acodec_parameter,
				"-vn", tmpfile_name.as_str()], None)
		}
		_ => cmd("convert", vec![&file_path, tmpfile_name.as_str()], None),
	};
	if output.as_str() != "" {
		dbg!("{}", output);
	}
	return Some(
		MediaConv {
			file_name: tmpfile_name,
			dir: tmpdir,
		}
	)
}

pub fn match_command(
	message: &Message,
	command: BotCmd,
) -> Option<Result<Response, Box<dyn Error>>> {
	match command {
		BotCmd::Chat(arg) => {
			// continue if chat_admin or private
			if message.chat.r#type.as_str() != "private" {
				let user_id = message.from.as_ref()?.id;
				let get_chat_member = GetChatMember {
					chat_id: message.chat.id,
					user_id,
				};
				let Some(chat_member) = get_chat_member.request() else {
					return None;
				};
				match chat_member.status.as_str() {
					"creator" | "administrator" => (),
					_ => return None,
				}
			}
			let bot_chat_id = message.chat.id;

			let mut send_message = SendMessage::reply(message);
			match arg {
				ChatArg::Create => {
					if !Path::new(BOT_CHATS_DIR).exists() {
						std::fs::create_dir_all(BOT_CHATS_DIR).ok();
					}
					let bot_chat_file = format!("{}{}", BOT_CHATS_DIR, bot_chat_id);
					if !Path::new(&bot_chat_file).exists() {
						let users: Vec<i64> = vec![bot_chat_id];
						serde_json::to_writer(
							utils::w_create_truncate(&bot_chat_file).unwrap(),
							&users,
						)
						.unwrap();
						send_message.text = format!("chat {bot_chat_id} created");
					} else {
						send_message.text = format!("chat {bot_chat_id} already exists");
					}
				}
				ChatArg::Delete => {
					let bot_chat_file = format!("{}{}", BOT_CHATS_DIR, bot_chat_id);

					if Path::new(&bot_chat_file).exists() {
						std::fs::remove_file(bot_chat_file).ok();
						send_message.text = format!("chat {bot_chat_id} deleted");
					} else {
						send_message.text = format!("chat {bot_chat_id} does not exists");
					}
				}
				ChatArg::Join(_) | ChatArg::Leave => match arg {
					ChatArg::Join(join_arg) => {
						let bot_chat_file_path = format!("{}{}", BOT_CHATS_DIR, join_arg);
						if Path::new(&bot_chat_file_path).exists() {
							let None = get_joined_bot_chat_users(bot_chat_id) else {
									return None;
								};
							let mut users = match serde_json::from_reader(fs::File::open(&bot_chat_file_path).unwrap()) {
								Ok(users) => users,
								Err(err) => {
									dbg!(err);
									vec![]
								}
							};
							users.push(bot_chat_id);
							serde_json::to_writer(fs::File::create(&bot_chat_file_path).unwrap(), &users).unwrap();

							send_message.text = format!("chat {} joined", join_arg);
						}
					}
					ChatArg::Leave => {
						let Some(mut bot_chat) = get_joined_bot_chat_users(bot_chat_id) else {
								return None;
							};
						let bot_chat_file_path = format!("{}{}", BOT_CHATS_DIR, bot_chat.host);
						bot_chat.users.retain(|&id| id != bot_chat_id);
						serde_json::to_writer(fs::File::create(&bot_chat_file_path).unwrap(), &bot_chat.users).unwrap();

						send_message.text = format!("chat {} exited", bot_chat.host);
					}
					_ => return None,
				},
				ChatArg::List => {
					let chat_list = std::fs::read_dir(BOT_CHATS_DIR).unwrap();

					send_message.text = format!("list of existing chats:\n");
					for x in chat_list {
						send_message.text +=
							format!("{}\n", x.unwrap().file_name().into_string().unwrap()).as_str();
					}
				}
			}
			return Some(send_message.request());
		}
		BotCmd::Convert(arg) => {
			let Some(media_conv) = get_media_conv(message.reply_to_message.as_ref()?, &arg) else {
				return None;
			};
			let request: Result<Response, Box<dyn Error>>;
			match arg {
				ConvertArg::Audio => {
					let mut method = SendAudio::reply(message);
					method.audio = media_conv.file_name;
					request = method.request();
				}
				ConvertArg::Photo => {
					let mut method = SendPhoto::reply(message);
					method.photo = media_conv.file_name;
					request = method.request();
				}
				ConvertArg::Sticker => {
					let mut method = SendSticker::reply(message);
					method.sticker = media_conv.file_name;
					request = method.request();
				}
				ConvertArg::Video => {
					let mut method = SendVideo::reply(message);
					method.video = media_conv.file_name;
					request = method.request();
				}
				ConvertArg::Animation => {
					let mut method = SendAnimation::reply(message);
					method.animation = media_conv.file_name;
					request = method.request();
				}
			}
			fs::remove_dir_all(media_conv.dir).unwrap();
			return Some(request);
		}
		BotCmd::Data(arg) => {
			let data = {
				match (arg, &message.reply_to_message) {
					(DataArg::User, Some(reply)) => format!("{:#?}", reply.from.as_ref()?),
					(DataArg::User, None) => format!("{:#?}", message.from.as_ref()?),
					(DataArg::Chat, Some(reply)) => format!("{:#?}", reply.chat),
					(DataArg::Chat, None) => format!("{:#?}", message.chat),
				}
			};

			let mut method = SendMessage::reply(message);
			method.text = ["<code>", data.as_str(), "</code>"].join("");
			method.parse_mode = Some(String::from("html"));
			return Some(method.request());
		}
		BotCmd::Dice(number) => {
			use std::collections::HashMap;
			let mut hm = HashMap::new();
			for x in 1..=number {
				hm.insert(x.to_string(), x);
			}

			let mut method = SendMessage::reply(message);
			method.text = hm.keys().next().unwrap().to_string();
			return Some(method.request());
		}
		BotCmd::Ping => {
			use std::time::Instant;
			let instant = Instant::now();
			GetMe::request();
			let get_me_time = format!("/getMe method time: {} μs", instant.elapsed().as_micros());

			let system_time: SystemTime = SystemTime::now();
			let epoch_secs = system_time.duration_since(UNIX_EPOCH).unwrap().as_secs();
			let response_time = epoch_secs - message.date;

			use crate::utils::lstatic::START_TIME;
			let exec_time = START_TIME.read().unwrap().elapsed().as_millis();

			let mut method = SendMessage::reply(message);
			method.text = format!(
				"pong\napi: {}\nresponse time: {} s\nexec time: {} ms",
				get_me_time, response_time, exec_time,
			);
			return Some(method.request());
		}
		BotCmd::Sed(arg) => {
			let reply_text = message.reply_to_message.as_ref()?.text.as_ref()?;

			let mut method = SendMessage::reply(message);
			method.text = cmd("sed", vec!["--sandbox", &arg], Some(reply_text.to_string()));
			return Some(method.request());
		}
		BotCmd::Source => {
			let mut zip = {
				zip::ZipWriter::new(
					std::fs::OpenOptions::new()
						.write(true)
						.create(true)
						.open("source.zip")
						.unwrap(),
				)
			};

			zip_file(&mut zip, "Cargo.toml");
			for path in utils::path("src/").iter() {
				zip_file(&mut zip, path.as_str());
			}

			zip.finish().unwrap();

			let mut send_document = SendDocument::reply(message);
			send_document.document = "source.zip".to_string();
			return Some(send_document.request());
		}
		BotCmd::Ban(chat_member) => {
			let mut ban = BanChatMember::default();
			ban.chat_id = message.chat.id;
			ban.user_id = chat_member.user.id;
			let ban_response = ban.request();
			let is_ok = match ban_response {
				Ok(ref ban_response) => ban_response.result["ok"].as_bool().unwrap(),
				Err(_) => false,
			};

			let mut send_message = SendMessage::reply(message);
			if is_ok {
				send_message.text = format!("user {} banned", chat_member.user.first_name);
			} else {
				send_message.text = format!("error: {:?}", ban_response);
			}
			return Some(send_message.request());
		}
		BotCmd::Mute(chat_member) => {
			let mut restrict = RestrictChatMember::default();
			restrict.chat_id = message.chat.id;
			restrict.user_id = chat_member.user.id;
			restrict.permissions.can_send_messages = Some(false);
			let restrict_response = restrict.request();
			let is_ok = match restrict_response {
				Ok(ref restrict_response) => restrict_response.result["ok"].as_bool().unwrap(),
				Err(_) => false,
			};

			let mut send_message = SendMessage::reply(message);
			if is_ok {
				send_message.text = format!("user {} muted", chat_member.user.first_name);
			} else {
				send_message.text = format!("error: {:?}", restrict_response);
			}
			return Some(send_message.request());
		}
	}
}

pub fn match_text(message: &Message, text: &str) -> Option<Result<Response, Box<dyn Error>>> {
	match text.to_lowercase().as_str() {
		"quanto fa" => {
			let reply_text: &String = message.reply_to_message.as_ref()?.text.as_ref()?;

			let signs = vec![" x ", " * ", " + ", " - ", " / "];
			let (vec_str, sign) = sign_split_vec_str(reply_text, signs)?;
			let vec_usize = chars_bytes_usize_sum(vec_str);

			let result = match sign {
				" x " | " * " => (vec_usize[0] * vec_usize[1]).to_string(),
				" + " => (vec_usize[0] + vec_usize[1]).to_string(),
				" - " => (vec_usize[0] as isize - vec_usize[1] as isize).to_string(),
				" / " => (vec_usize[0] as f64 / vec_usize[1] as f64).to_string(),
				_ => return None,
			};

			let mut method = SendMessage::reply(message);
			method.text = result;
			return Some(method.request());
		}
		_ => return None,
	}
}

pub fn match_chat_id(message: &Message) -> Option<Result<Response, Box<dyn Error>>> {
	match message.chat.id {
		-1001428507662 => {
			// test chat
			if message.text.as_ref()? != "owo" {
				return None;
			}

			let mut method = SendMessage::reply(message);
			method.text = "uwu".to_string();
			return Some(method.request());
		}
		_ => return None,
	}
}

fn get_joined_bot_chat_users(bot_chat_id: i64) -> Option<BotChat> {
	let mut users: Vec<i64>;
	let host: i64;
	let chat_list = std::fs::read_dir(BOT_CHATS_DIR).unwrap();
	for chat_id in chat_list {
		let Ok(chat_id) = chat_id else {
			continue;
		};
		let Ok(file_type) = chat_id.file_type() else {
			continue;
		};
		if file_type.is_dir() {
			continue;
		}
		let bot_chat_file_path = format!(
			"{}{}",
			BOT_CHATS_DIR,
			chat_id.file_name().into_string().unwrap()
		);
		if !Path::new(&bot_chat_file_path).exists() {
			continue;
		}
		let bot_chat_file = fs::File::open(&bot_chat_file_path).unwrap();
		users = match serde_json::from_reader(bot_chat_file) {
			Ok(users) => users,
			Err(_) => vec![],
		};
		for user_id in &users {
			if user_id == &bot_chat_id {
				host = chat_id.file_name().into_string().unwrap().parse().unwrap();
				return Some(BotChat { host, users });
			}
		}
	}
	return None;
}

pub fn match_bot_chat_id(message: &Message) -> Option<Result<Response, Box<dyn Error>>> {
	let (method, input_id, caption): (&str, &str, &Option<String>) = {
		if let Some(text) = &message.text {
			("message", &text, &message.caption)
		} else if let Some(sticker) = &message.sticker {
			("sticker", &sticker.file_id, &message.caption)
		} else if let Some(video) = &message.video {
			("video", &video.file_id, &message.caption)
		} else if let Some(animation) = &message.animation {
			("animation", &animation.file_id, &message.caption)
		} else if let Some(photo) = &message.photo {
			let photo = &photo[photo.len() - 1];
			("photo", &photo.file_id, &message.caption)
		} else {
			return None;
		}
	};
	let Some(mut bot_chat) = get_joined_bot_chat_users(message.chat.id) else {
		return None;
	};
	bot_chat.users.retain(|&id| id != message.chat.id);
	
	for chat_id in bot_chat.users {
		let chat_id = chat_id.clone();
		let input_id = input_id.to_string();
		let response = match method {
			"message" => {
				let mut method = SendMessage::default();
				method.chat_id = chat_id;
				method.text = input_id.clone();
				method.request()
			}
			"photo" => {
				let mut method = SendPhoto::default();
				method.chat_id = chat_id;
				method.photo = input_id.clone();
				method.caption = caption.clone();
				method.request()
			}
			"sticker" => {
				let mut method = SendSticker::default();
				method.chat_id = chat_id;
				method.sticker = input_id.clone();
				method.request()
			}
			"video" => {
				let mut method = SendVideo::default();
				method.chat_id = chat_id;
				method.video = input_id.clone();
				method.caption = caption.clone();
				method.request()
			}
			"animation" => {
				let mut method = SendAnimation::default();
				method.chat_id = chat_id;
				method.animation = input_id.clone();
				method.caption = caption.clone();
				method.request()
			}
			_ => return None,
		};
		if let Ok(response) = response {
			match serde_json::from_value::<Message>(response.result) {
				Ok(_) => (),
				Err(err) => {
					dbg!(err);
					return None;
				}
			}
		}
	}
	return None;
}
