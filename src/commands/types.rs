use crate::{methods::GetChatMember, types::ChatMember, Message};

pub enum BotCmd {
	Ban(ChatMember),
	Chat(ChatArg),
	Convert(ConvertArg),
	Data(DataArg),
	Dice(u64),
	Mute(ChatMember),
	Ping,
	Sed(String),
	Source,
}
pub enum ChatArg {
	Create,
	Delete,
	Join(i64),
	Leave,
	List,
}
pub enum ConvertArg {
	Animation,
	Audio,
	Video,
	Photo,
	Sticker,
}
pub enum DataArg {
	Chat,
	User,
}

impl BotCmd {
	pub fn get_cmd(message: &Message) -> Option<Self> {
		let Some(ref text) = message.text else {
			return None;
		};

		let cmd: String;
		let args: String;
		if text.starts_with([';', '/']) {
			let mut iterator = text.splitn(2, char::is_whitespace);
			cmd = iterator.next().unwrap()[1..].to_string();
			args = iterator.next().unwrap_or_default().to_string();
		} else {
			return None;
		}
		match cmd.to_lowercase().as_str() {
			"ban" | "mute" => {
				let user_id = if let Ok(user_id) = args.split_whitespace().next()?.parse::<i64>() {
					user_id
				} else {
					message.from.as_ref()?.id
				};
				let get_chat_member = GetChatMember {
					user_id,
					chat_id: message.chat.id,
				};
				let Some(chat_member) = get_chat_member.request() else {
					return None;
				};
				match cmd.to_lowercase().as_str() {
					"ban" => return Some(BotCmd::Ban(chat_member)),
					"mute" => return Some(BotCmd::Mute(chat_member)),
					_ => return None,
				}
			}
			"chat" => {
				let mut args = args.split_whitespace();
				let Some(arg) = args.next() else {
					return None;
				};
				match arg.to_lowercase().as_str() {
					"create" => return Some(BotCmd::Chat(ChatArg::Create)),
					"delete" => return Some(BotCmd::Chat(ChatArg::Delete)),
					"join" | "leave" => {
						let chat_id = match args.next()?.parse::<i64>() {
							Ok(chat_id) => chat_id,
							Err(err) => {
								dbg!(err);
								return None;
							}
						};
						match arg.to_lowercase().as_str() {
							"join" => return Some(BotCmd::Chat(ChatArg::Join(chat_id))),
							"leave" => return Some(BotCmd::Chat(ChatArg::Leave)),
							_ => return None,
						}
					}
					"list" => return Some(BotCmd::Chat(ChatArg::List)),
					_ => return None,
				}
			}
			"convert" => {
				let Some(arg) = args.split_whitespace().next() else {
					return None;
				};
				match arg.to_lowercase().as_str() {
					"photo" => return Some(BotCmd::Convert(ConvertArg::Photo)),
					"sticker" => return Some(BotCmd::Convert(ConvertArg::Sticker)),
					"animation" => return Some(BotCmd::Convert(ConvertArg::Animation)),
					"audio" => return Some(BotCmd::Convert(ConvertArg::Audio)),
					"video" => return Some(BotCmd::Convert(ConvertArg::Video)),
					_ => return None,
				}
			}
			"data" => {
				let Some(arg) = args.split_whitespace().next() else {
					return None;
				};
				match arg.to_lowercase().as_str() {
					"chat" => return Some(BotCmd::Data(DataArg::Chat)),
					"user" => return Some(BotCmd::Data(DataArg::User)),
					_ => return None,
				}
			}
			"dice" => {
				let Some(arg) = args.split_whitespace().next() else {
					return None;
				};
				if let Ok(arg) = arg.parse::<u64>() {
					return Some(BotCmd::Dice(arg));
				} else {
					return None;
				}
			}
			"ping" => return Some(BotCmd::Ping),
			"sed" => {
				if args != String::new() {
					return Some(BotCmd::Sed(args));
				} else {
					return None;
				}
			}
			_ => return None,
		}
	}
}
