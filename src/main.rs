use serde::Deserialize;
use std::env;
use std::error::Error;
use std::fs::File;
use std::io::Write;
use std::net::{TcpListener, TcpStream};
use telerusteek::requests::stream_read_json;
use telerusteek::utils::lstatic::{API, START_TIME};

fn handle_connection(mut stream: TcpStream) -> Result<(), Box<dyn Error>> {
	let mut start_time = START_TIME.write()?;
	*start_time = std::time::Instant::now();

	let json = stream_read_json(&mut stream)?;
	let header = b"HTTP/1.1 204 No Content\r\n\r\n";
	stream.write_all(header)?;

	std::thread::spawn(move || {
		telerusteek::parse_json::get_message(&json);
	});
	return Ok(());
}

fn main() {
	lazy_static::initialize(&API);
	let args: Vec<String> = env::args().collect();
	println!("{:?}", args);

	#[derive(Debug, Deserialize)]
	struct WebHook {
		host: String,
		port: String,
	}

	if args.contains(&"--polling".to_owned()) {
		for line in std::io::stdin().lines().map_while(Result::ok) {
			std::thread::spawn(move || {
				telerusteek::parse_json::get_message(&line);
			});
		}
	} else {
		let config: WebHook = serde_yaml::from_reader(File::open("webhook.yaml").unwrap()).unwrap();
		let listener = TcpListener::bind(config.host + ":" + &config.port).unwrap();

		for maybe_stream in listener.incoming() {
			let Ok(stream) = maybe_stream else {
				continue;
			};

			if let Err(error) = handle_connection(stream) {
				eprintln!("Error handling connection: {}", error);
			}
		}
	}
}
