use std::fs;
use std::io::Error;
use std::path::Path;

fn r_dir(files: &mut Vec<String>, dir: &Path, parent: &Path) {
	for entry in fs::read_dir(dir).unwrap() {
		let dir = entry.unwrap();
		let file_type = {
			if dir.file_type().unwrap().is_file() {
				"file"
			} else if dir.file_type().unwrap().is_dir() {
				"directory"
			} else {
				"symlink"
			}
		};

		if file_type == "directory" {
			let path = &dir.path();
			r_dir(files, path.as_path(), path.as_path());
		} else {
			files.push(format!(
				"{}/{}",
				parent.display(),
				dir.file_name().into_string().unwrap()
			));
		}
	}
}

pub fn read_append_create(file: &str) -> Result<std::fs::File, Error> {
	std::fs::OpenOptions::new()
		.read(true)
		.append(true)
		.create(true)
		.open(file)
}

pub fn w_create_truncate(file: &str) -> Result<std::fs::File, Error> {
	std::fs::OpenOptions::new()
		.write(true)
		.truncate(true)
		.create(true)
		.open(file)
}

pub fn path(path: &str) -> Vec<String> {
	let mut files: Vec<String> = vec![];
	let start_path = Path::new(path);
	r_dir(&mut files, start_path, start_path);
	files
}
