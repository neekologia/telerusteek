use crate::types::Api;
use lazy_static::lazy_static;
use std::fs::File;
use std::sync::{Arc, RwLock};
use std::time::Instant;

fn make_tls_config() -> Arc<rustls::ClientConfig> {
	let mut root_store = rustls::RootCertStore::empty();
	root_store.add_server_trust_anchors(webpki_roots::TLS_SERVER_ROOTS.0.iter().map(|ta| {
		rustls::OwnedTrustAnchor::from_subject_spki_name_constraints(
			ta.subject,
			ta.spki,
			ta.name_constraints,
		)
	}));
	let config = rustls::ClientConfig::builder()
		.with_safe_defaults()
		.with_root_certificates(root_store)
		.with_no_client_auth();

	return Arc::new(config);
}

lazy_static! {
	pub static ref START_TIME: RwLock<Instant> = RwLock::new(Instant::now());
	pub static ref API: Api = serde_yaml::from_reader(File::open("api.yaml").unwrap()).unwrap();
	pub static ref TLSCONFIG: Arc<rustls::ClientConfig> = make_tls_config();
}

pub const BOT_CHATS_DIR: &str = "db/bot_chats/";
