use crate::{
	types::Response,
	utils::lstatic::{API, TLSCONFIG},
};
use serde_json;
use serde_json::Value;
use std::{
	convert::TryInto,
	error::Error,
	fs::File,
	io::{Read, Write},
	net::TcpStream,
	str,
	time::Duration,
};

pub fn stream_read_json<S: std::io::Read>(stream: &mut S) -> Result<String, Box<dyn Error>> {
	let mut j = false;
	let mut buffer = [0; 64000];
	let mut string = String::new();
	let mut json = String::new();

	let data = stream.read(&mut buffer[0..64000]);
	match data {
		Ok(data) => {
			string = String::from_utf8_lossy(&buffer[0..data]).to_string();
		}
		Err(err) => {
			eprintln!("error: {:?}", err);
		}
	}
	for &item in string.as_bytes().iter() {
		if j {
			json.push(item as char);
		} else if item == b'{' {
			json.push(item as char);
			j = true;
		}
	}
	if !j {
		let err = format!("json not found, string: {}", string);
		return Err(err.into());
	}

	return Ok(json.trim_matches(char::from(0)).to_string());
}

pub fn post(
	method: &str,
	json: String,
	input_file: Option<String>,
) -> Result<Response, Box<dyn Error>> {
	let mut file_bytes = vec![];

	let upload = {
		match input_file {
			Some(ref file) => {
				let mut f = File::open(file.as_str()).unwrap();
				f.read_to_end(&mut file_bytes).unwrap();
				true
			}
			None => false,
		}
	};

	let boundary = b"--------e4ef8847482d240d0a3ac612876ba8d1";
	let mut up_bytes = vec![];
	if upload {
		let filename = input_file.unwrap();
		let params: Value = serde_json::from_str(&json).unwrap();
		up_bytes.write_all(b"--").unwrap();
		up_bytes.write_all(boundary).unwrap();
		for (key, value) in params.as_object().unwrap() {
			up_bytes
				.write_all(b"\r\nContent-Disposition: form-data; name=\"")
				.unwrap();
			up_bytes.write_all(key.as_bytes()).unwrap();
			match key.as_str() {
				"document" | "video" | "photo" | "sticker" | "audio" => {
					up_bytes.write_all(b"\"").unwrap();
					up_bytes.write_all(b"; filename=\"").unwrap();
					up_bytes.write_all(filename.as_bytes()).unwrap();
					up_bytes.write_all(b"\"\r\n").unwrap();
					up_bytes
						.write_all(b"Content-Type: application/octet-stream\r\n\r\n")
						.unwrap();
					up_bytes.write_all(&file_bytes).unwrap();
				}
				_ => {
					up_bytes.write_all(b"\"\r\n\r\n").unwrap();
					up_bytes.write_all(value.to_string().as_bytes()).unwrap();
				}
			}
			up_bytes.write_all(b"\r\n--").unwrap();
			up_bytes.write_all(boundary).unwrap();
		}
		up_bytes.write_all(b"--").unwrap();
	}
	let post_request = format!("/bot{}{}", API.token, method);
	let addr = API.host.clone() + ":" + &API.port;

	let mut headers = vec![];
	headers.write_all(b"POST ").unwrap();
	headers.write_all(post_request.as_bytes()).unwrap();
	headers.write_all(b" HTTP/1.1\r\n").unwrap();
	write!(&mut headers, "Host: {}\r\n", addr).unwrap();
	headers
		.write_all(b"User-Agent: telerusteek/1.0\r\nAccept: */*\r\n")
		.unwrap();

	let mut post = vec![];
	post.write_all(&headers).unwrap();
	post.write_all(b"Content-Length: ").unwrap();
	if upload {
		post.write_all(up_bytes.len().to_string().as_bytes())
			.unwrap();
		post.write_all(b"\r\nContent-Type: multipart/form-data; boundary=")
			.unwrap();
		post.write_all(boundary).unwrap();
		post.write_all(b"\r\n\r\n").unwrap();
		post.write_all(&up_bytes).unwrap();
	} else {
		post.write_all(json.len().to_string().as_bytes()).unwrap();
		post.write_all(b"\r\nContent-Type: application/json\r\n\r\n")
			.unwrap();
		post.write_all(json.as_bytes()).unwrap();
	}

	let mut socket = TcpStream::connect(addr)?;
	if API.tls {
		let mut connection =
			rustls::ClientConnection::new((*TLSCONFIG).clone(), (&*API.host).try_into().unwrap())
				.unwrap();
		let mut stream = rustls::Stream::new(&mut connection, &mut socket);
		return write_and_read(&mut stream, &post);
	} else {
		return write_and_read(&mut socket, &post);
	}
}

fn write_and_read<S: Write + Read>(stream: &mut S, data: &[u8]) -> Result<Response, Box<dyn Error>> {
	stream.write_all(data)?;

	let api_response = stream_read_json(stream)?;
	if let Ok(json) = serde_json::from_str(&api_response) {
		return Ok(json);
	} else {
		return Err(api_response.into());
	}
}

pub fn post_with_wait(
	method: &str,
	json: String,
	input_file: Option<String>,
) -> Result<Response, Box<dyn Error>> {
	loop {
		let post_response = post(method, json.clone(), input_file.clone())?;
		let Some(ref parameters) = post_response.parameters else {
				return Ok(post_response)
			};
		let Some(retry_after) = parameters.retry_after else {
				return Ok(post_response);
			};
		std::thread::sleep(Duration::from_secs(retry_after));
		continue;
	}
}
